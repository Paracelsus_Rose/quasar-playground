import { ref, computed } from 'vue'

const leftDrawerOpen = ref(true)
const leftDrawerStyle = computed(() => {
  return 'height: calc(100vh - 50px)'
})

export default function useLeftDrawer() {
  const toggleLeftDrawer = () => (leftDrawerOpen.value = !leftDrawerOpen.value)

  return {
    leftDrawerOpen,
    leftDrawerStyle,
    toggleLeftDrawer
  }
}
