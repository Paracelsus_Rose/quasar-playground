import { ref, computed } from 'vue'
import { uid, Dialog } from 'quasar'
import AddTestDialog from 'components/AddTestDialog.vue'

const sortFn = (a, b) => b.DateCreated - a.DateCreated
const latestFilterFn = (t, index) => index < 10
const archivedFilterFn = t => t.Archived
const currentFilterFn = t => !t.Archived

const allTests = ref([])

const tests = computed(() => allTests.value.filter(currentFilterFn))
const archivedTests = computed(() => allTests.value.filter(archivedFilterFn))
const latestTests = computed(() => tests.value.sort(sortFn).filter(latestFilterFn))

export const columns = [
  {
    name: 'name',
    field: 'name',
    label: 'Name',
    align: 'left',
  },
  {
    name: 'description',
    field: 'description',
    label: 'Description',
    align: 'left'
  },
  {
    name: 'actions',
    field: 'actions',
    label: 'Actions',
    align: 'left'
  }
]

export default function useTests() {
  const addNewTest = test => {
    allTests.value.push({
      ...test,
      DateCreated: +new Date(),
      Archived: false,
      DateArchived: null,
    })
  }

  const deleteTest = testId => {
    allTests.value = allTests.value.filter(t => t.Id !== testId) //little confusion here
  }

  const archiveTest = testId => {
    const test = allTests.value.find(t => t.id === testId)
    if (!test) return
    test.Archived = true
    test.DateArchived = +new Date()
  }

	const addRandomTest = () => {
		const id = uid()
		addNewTest({
			Id: id,
			Name: `Test ${id.substring(0, 8)}`,
			Description: `Test ${id}`,
		})
	}

	const showAddDialog = () => {
		Dialog.create({
			component: AddTestDialog,
			// componentProps: { successCallback },
		})
	}

  return {
    tests,
    latestTests,
    allTests,
    archivedTests,
    addNewTest,
    deleteTest,
    addRandomTest,
    showAddDialog
  }


}
